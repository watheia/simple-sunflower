# ✨ strapiweb

This ~~is~~ was Stackbit's "Starter" theme built with [Next.js](https://nextjs.org/) and
powered by content stored in files.

Click the button below to create a new website from this theme using Stackbit:

Now it's this:

![depgraph](./depgraph.svg)
