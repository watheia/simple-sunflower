const sourcebit = require("sourcebit")
// const withTM = require("next-transpile-modules")([
//   "@adobe/react-spectrum",
//   "@react-spectrum/actiongroup",
//   "@react-spectrum/buttongroup",
//   "@react-spectrum/divider",
//   "@react-spectrum/illustratedmessage",
//   "@react-spectrum/layout",
//   "@react-spectrum/menu",
//   "@react-spectrum/overlays",
//   "@react-spectrum/provider",
//   "@react-spectrum/slider",
//   "@react-spectrum/tabs",
//   "@react-spectrum/theme-dark",
//   "@react-spectrum/tooltip",
//   "@react-spectrum/well",
//   "@react-spectrum/breadcrumbs",
//   "@react-spectrum/checkbox",
//   "@react-spectrum/form",
//   "@react-spectrum/image",
//   "@react-spectrum/link",
//   "@react-spectrum/meter",
//   "@react-spectrum/picker",
//   "@react-spectrum/radio",
//   "@react-spectrum/statuslight",
//   "@react-spectrum/text",
//   "@react-spectrum/theme-default",
//   "@react-spectrum/utils",
//   "@react-spectrum/button",
//   "@react-spectrum/dialog",
//   "@react-spectrum/icon",
//   "@react-spectrum/label",
//   "@react-spectrum/listbox",
//   "@react-spectrum/numberfield",
//   "@react-spectrum/progress",
//   "@react-spectrum/searchfield",
//   "@react-spectrum/switch",
//   "@react-spectrum/textfield",
//   "@react-spectrum/theme-light",
//   "@react-spectrum/view"
// ])
const sourcebitConfig = require("./sourcebit.js")

sourcebit.fetch(sourcebitConfig)

module.exports = {
  trailingSlash: true,
  devIndicators: {
    autoPrerender: false
  },
  webpack: (config, { webpack }) => {
    // Tell webpack to ignore watching content files in the content folder.
    // Otherwise webpack receompiles the app and refreshes the whole page.
    // Instead, the src/pages/[...slug].js uses the "withRemoteDataUpdates"
    // function to update the content on the page without refreshing the
    // whole page
    config.plugins.push(new webpack.WatchIgnorePlugin({ paths: [/\/content\//] }))
    return config
  }
}
