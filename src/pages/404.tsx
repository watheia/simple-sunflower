/**
 * Caution: Consider this file when using NextJS or GatsbyJS
 *
 * You may delete this file and its occurrences from the project filesystem if you are using react-scripts
 */
import React from "react"
import NotFoundCover from "@watheia/agency.views.not-found-cover"
import Minimal from "@waweb/base-ui.layout.page.minimal"
import { WithLayout } from "@waweb/base-ui.layout.with-layout"

const FourOFourPage = (): JSX.Element => {
  return <WithLayout component={NotFoundCover} layout={Minimal} />
}

export default FourOFourPage
