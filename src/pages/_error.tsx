import React from "react"
import ServerError from "@watheia/agency.views.server-error"
import Minimal from "@waweb/base-ui.layout.page.minimal"
import WithLayout from "@waweb/base-ui.layout.with-layout"

const ErrorPage = (): JSX.Element => {
  return <WithLayout component={ServerError} layout={Minimal} />
}

export default ErrorPage
