/**
 * Caution: Consider this file when using NextJS
 *
 * You may delete this file and its occurrences from the project filesystem if you are using GatsbyJS or react-scripts version
 */
import React from "react"
import Head from "next/head"
import { ThemeProvider } from "@waweb/base-ui.theme.theme-provider"
import "react-lazy-load-image-component/src/effects/opacity.css"
import "leaflet/dist/leaflet.css"
import "swiper/swiper-bundle.min.css"
import "aos/dist/aos.css"
import "@styles/main.scss"
import "@spectrum-css/vars/dist/spectrum-global.css"
import "@spectrum-css/vars/dist/spectrum-medium.css"
import "@spectrum-css/vars/dist/spectrum-light.css"
// import "@spectrum-css/vars/dist/spectrum-dark.css"
import "@spectrum-css/page/dist/index-vars.css"

interface AppProps {
  Component: any
  pageProps: any
}

export default function App({ Component, pageProps }: AppProps): JSX.Element {
  return (
    <ThemeProvider>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <link href="https://cdn.watheia.org/assets/typography.css" rel="stylesheet" />
      </Head>
      <Component {...pageProps} />
    </ThemeProvider>
  )
}
