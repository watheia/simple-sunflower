import React from "react"
import NextLink from "next/link"
import { LinkProps as NextLinkProps } from "next/link"

export type LinkProps = React.PropsWithChildren<NextLinkProps> &
  React.HtmlHTMLAttributes<HTMLAnchorElement>

export default function Link({ children, href, ...other }: LinkProps): JSX.Element {
  // Pass Any internal link to Next.js Link, for anything else, use <a> tag
  const internal = /^\/(?!\/)/.test(href as string)
  if (internal) {
    return (
      <NextLink href={href}>
        <a {...other}>{children}</a>
      </NextLink>
    )
  }

  return (
    <a href={href as string} {...other}>
      {children}
    </a>
  )
}
