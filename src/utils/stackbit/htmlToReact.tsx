import React from "react"
import ReactHtmlParser, { convertNodeToElement } from "react-html-parser"
import ScriptTag from "react-script-tag"
import Link from "./link"
import { noop, map, isEmpty, omit } from "lodash"

const convertChildren = (children: any, index: number) =>
  map(children, (childNode) => convertNodeToElement(childNode, index, noop))

/**
 * **UNSAFE** Use trusted content only to avoid XSS
 * Constructs JSX nodes from a raw markdown string.
 *
 * @param html
 * @returns
 */
export default function htmlToReact(
  html: string
): React.ReactElement<any, string | React.JSXElementConstructor<any>>[] | null {
  if (!html) {
    return null
  }
  return ReactHtmlParser(html, {
    transform: (node, index) => {
      if (node.type === "script") {
        if (!isEmpty(node.children)) {
          return (
            <ScriptTag key={index} {...node.attribs}>
              {convertChildren(node.children, index)}
            </ScriptTag>
          )
        } else {
          return <ScriptTag key={index} {...node.attribs} />
        }
      } else if (node.type === "tag" && node.name === "a") {
        const href = node.attribs.href
        const props = omit(node.attribs, "href")
        // use Link only if there are no custom attributes like style, class, and what's not that might break react
        if (isEmpty(props)) {
          return (
            <Link key={index} href={href} {...props}>
              {convertChildren(node.children, index)}
            </Link>
          )
        }
      }
    }
  })
}
