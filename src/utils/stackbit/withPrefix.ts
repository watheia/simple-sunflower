import { trim, compact, startsWith, trimStart } from "lodash"
import { pathPrefix } from "./constants"

export default function withPrefix(url: string | undefined): string {
  if (!url) {
    return ""
  }

  if (startsWith(url, "#") || startsWith(url, "http://") || startsWith(url, "https://")) {
    return url
  }
  const basePath = trim(pathPrefix, "/")
  return "/" + compact([basePath, trimStart(url, "/")]).join("/")
}
