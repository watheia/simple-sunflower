import { trim, startsWith, get } from "lodash"

/**
 *
 * @param props Load data at the relative content path
 * @param dataPath
 * @returns
 */
export default function getData(
  props: Record<string, any>,
  dataPath: string | undefined
): any {
  dataPath = trim(dataPath, "/")
  if (startsWith(dataPath, "content/data/")) {
    dataPath = dataPath.replace("content/data/", "")
  }
  // remove extension
  dataPath = dataPath.replace(/\.\w+$/, "")
  const path = dataPath.split("/")
  return get(props, path)
}
