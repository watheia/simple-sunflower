import classnames from "classnames"

/**
 * A simple wrapper around classNames to return null, if no classes were generated
 * Otherwise, original classNames returns empty string which causes class="" to be generated
 *
 * @param this
 * @param args
 * @returns
 */
export default function classNames(this: any, ...args: any[]): string | undefined {
  return classnames.call(this, ...args) || undefined
}
