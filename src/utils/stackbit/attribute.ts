/**
 * Conditional attribute decoration.
 *
 * @param name
 * @param value
 * @param condition
 * @returns
 */
export default function attribute(
  name: string,
  value: any,
  condition: boolean
): { [x: string]: any } | null {
  if (typeof condition === "undefined") {
    condition = true
  }
  return condition ? { [name]: value } : null
}
