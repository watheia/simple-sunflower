import marked from "marked"
import { JSXElementConstructor, ReactElement } from "react"
import htmlToReact from "./htmlToReact"

export default function markdownify(
  markdown: string
): ReactElement<any, string | JSXElementConstructor<any>>[] | null {
  if (!markdown) {
    return null
  }
  return htmlToReact(marked(markdown))
}
