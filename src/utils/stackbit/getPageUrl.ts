import { get } from "lodash"
import withPrefix from "./withPrefix"

/**
 * Get the full URL for a page
 *
 * @param post
 * @param param1
 * @returns
 */
export default function getPageUrl(post: any, { withPrefix: addPrefix = false } = {}): any {
  const urlPath = get(post, "__metadata.urlPath")
  return addPrefix ? withPrefix(urlPath) : urlPath
}
