import React from "react"
import { get, map } from "lodash"
import { sectionsByTypeName, Layout } from "@components/index"

export default class Advanced extends React.Component {
  render(): JSX.Element {
    const data = get(this.props, "data")
    const config = get(data, "config")
    const posts = get(this.props, "posts")
    const page = get(this.props, "page")
    const sections = get(page, "sections")
    return (
      <Layout page={page} config={config}>
        {map(sections, (section, index) => {
          const sectionType = get(section, "type")
          const Section = get(sectionsByTypeName, sectionType)
          if (!Section) {
            throw new Error(`no component matching the page section's type: ${sectionType}`)
          }
          return <Section key={index} section={section} data={data} posts={posts} />
        })}
      </Layout>
    )
  }
}
