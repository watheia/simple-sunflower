import React from "react"
import { get } from "lodash"

import Layout from "@components/Layout"
import { markdownify } from "@watheia/utils.stackbit"

export default class Page extends React.Component {
  render() {
    const data = get(this.props, "data")
    const config = get(data, "config")
    const page = get(this.props, "page")
    const title = get(page, "title")
    const markdownContent = get(page, "markdown_content")

    return (
      <Layout page={page} config={config}>
        <article className="page">
          <div className="container container--md">
            {title && (
              <header className="page__header">
                <h1 className="page__title">{title}</h1>
              </header>
            )}
            {markdownContent && (
              <div className="page__copy">{markdownify(markdownContent)}</div>
            )}
          </div>
        </article>
      </Layout>
    )
  }
}
