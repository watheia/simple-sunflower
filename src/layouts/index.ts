import advanced from "./advanced"
import page from "./page"
import post from "./post"

export const pageLayouts = {
  advanced,
  page,
  post
}
