import React from "react"
import { Helmet } from "react-helmet"
import { get, isEmpty, trim } from "lodash"

import { withPrefix } from "@watheia/utils.stackbit"
import Header from "./Header"
import Footer from "./Footer"

export default class Body extends React.Component<{ page: any; config: any }> {
  render() {
    const page = get(this.props, "page")
    const pageTitle = get(page, "title")
    const config = get(this.props, "config")
    const configTitle = get(config, "title")
    const favicon = get(config, "favicon")
    const domain = trim(get(config, "domain", ""), "/")
    const seo = get(page, "seo")
    const seoTitle = get(seo, "title")
    const title = seoTitle ? seoTitle : [pageTitle, configTitle].join(" | ")
    const seoDescription = get(seo, "description", "")
    const seoRobots = get(seo, "robots", []).join(",")
    const seoExtra = get(seo, "extra", []).map(
      (meta: any, index: React.Key | null | undefined) => {
        const keyName = get(meta, "keyName", "name")
        const name = get(meta, "name")
        if (!name) {
          return null
        }
        const nameAttr = { [keyName]: name }
        const relativeUrl = get(meta, "relativeUrl")
        let value = get(meta, "value")
        if (!value) {
          return null
        }
        if (relativeUrl) {
          if (!domain) {
            return null
          }
          value = domain + withPrefix(value)
        }
        return <meta key={index} {...nameAttr} content={value} />
      }
    )

    return (
      <React.Fragment>
        <Helmet>
          <title>{title}</title>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta name="google" content="notranslate" />
          <meta name="description" content={seoDescription} />
          {!isEmpty(seoRobots) && <meta name="robots" content={seoRobots} />}
          {seoExtra}
          {favicon && <link rel="icon" href={withPrefix(favicon)} />}
        </Helmet>
        <div id="site-wrap" className="site">
          <Header page={page} config={config} />
          <main id="content" className="site-content">
            {this.props.children}
          </main>
          <Footer config={config} />
        </div>
      </React.Fragment>
    )
  }
}
