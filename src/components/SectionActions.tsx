import React from "react"
import { map, get } from "lodash"

import Action from "./Action"

export default class SectionActions extends React.Component<{ actions: any }> {
  render() {
    const actions = get(this.props, "actions")
    return map(actions, (action, index) => <Action key={index} action={action} />)
  }
}
