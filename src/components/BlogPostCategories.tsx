import React, { HTMLAttributes } from "react"
import { get, map, size } from "lodash"

import { getData, Link, withPrefix } from "@watheia/utils.stackbit"

export type BlogPostCategoriesProps = {
  data: any
  categories: any
  containerClass: any
} & HTMLAttributes<HTMLDivElement>

export default class BlogPostCategories extends React.Component<BlogPostCategoriesProps> {
  renderCategory(
    categoryRef: string | undefined,
    categoryLength: number,
    index: number,
    data: Record<string, any>
  ): JSX.Element | null {
    const category = getData(data, categoryRef)
    if (!category) {
      return null
    }
    if (category.link) {
      return (
        <React.Fragment key={index}>
          <Link href={withPrefix(category.link)}>{category.title}</Link>
          {index < categoryLength - 1 && ", "}
        </React.Fragment>
      )
    } else {
      return (
        <React.Fragment key={index}>
          <span>{category.title}</span>
          {index < categoryLength - 1 && ", "}
        </React.Fragment>
      )
    }
  }

  render() {
    const data = get(this.props, "data")
    const categories = get(this.props, "categories")
    const containerClass = get(this.props, "containerClass", "")
    const categoryLength = size(categories)
    if (!categoryLength) {
      return null
    }
    return (
      <div className={containerClass}>
        <span>In </span>
        {map(categories, (categoryRef, index: number) =>
          this.renderCategory(categoryRef, categoryLength, index, data)
        )}
      </div>
    )
  }
}
