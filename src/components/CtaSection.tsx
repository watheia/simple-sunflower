import React from "react"
import { get, isEmpty } from "lodash"

import { classNames } from "@watheia/utils.stackbit"
import SectionActions from "./SectionActions"

export default class CtaSection extends React.Component {
  render() {
    const section = get(this.props, "section")
    const title = get(section, "title")
    const subtitle = get(section, "subtitle")
    const actions = get(section, "actions")
    const hasBackground = get(section, "has_background")
    const backgroundColor = get(section, "background_color", "white")

    return (
      <section className="section section--cta">
        <div className="container container--lg">
          <div
            className={classNames("section__body", "align-center", {
              inverse: hasBackground && backgroundColor === "blue",
              "bg-blue": hasBackground && backgroundColor === "blue",
              "bg-gray": hasBackground && backgroundColor === "gray"
            })}
          >
            <div className="container container--md">
              {title && <h2 className="section__title">{title}</h2>}
              {subtitle && (
                <div className="section__copy">
                  <p>{subtitle}</p>
                </div>
              )}
              {!isEmpty(actions) && (
                <div className="section__actions btn-group">
                  <SectionActions actions={actions} />
                </div>
              )}
            </div>
          </div>
        </div>
      </section>
    )
  }
}
