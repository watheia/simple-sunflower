import React from 'react';
import { render } from '@testing-library/react';
import { BasicWebBasic } from './web-basic.composition';

it('should render with the correct text', () => {
  const { getByText } = render(<BasicWebBasic />);
  const rendered = getByText('hello from WebBasic');
  expect(rendered).toBeTruthy();
});
