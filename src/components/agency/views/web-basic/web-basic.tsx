import React from 'react';

export type WebBasicProps = {
  /**
   * a text to be rendered in the component.
   */
  text: string
};

export function WebBasic({ text }: WebBasicProps) {
  return (
    <div>
      {text}
    </div>
  );
}
