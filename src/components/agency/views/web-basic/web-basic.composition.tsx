import React from 'react';
import { WebBasic } from './web-basic';

export const BasicWebBasic = () => (
  <WebBasic text="hello from WebBasic" />
);
