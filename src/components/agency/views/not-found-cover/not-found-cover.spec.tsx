import React from "react"
import { render } from "@testing-library/react"
import { NotFoundCoverView } from "./not-found-cover.composition"

it("should render with the correct text", () => {
  const { getByText } = render(<NotFoundCoverView />)
  const rendered = getByText(
    "There’s nothing here, but if you feel this is an error please"
  )
  expect(rendered).toBeTruthy()
})
