import React from "react"
import { render } from "@testing-library/react"
import { DefaultServerError } from "./server-error.composition"

it("should render without crashing", () => {
  const { getByTestId } = render(<DefaultServerError />)
  const rendered = getByTestId("server-error")
  expect(rendered).toBeTruthy()
})
