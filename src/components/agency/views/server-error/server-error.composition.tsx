import React from "react"
import ServerError from "./server-error"

export const DefaultServerError = () => <ServerError data-testid="server-error" />
