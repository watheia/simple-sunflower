import React from "react"
import { get } from "lodash"

import { markdownify } from "@watheia/utils.stackbit"

export default class ContentSection extends React.Component {
  render() {
    const section = get(this.props, "section")
    const title = get(section, "title")
    const content = get(section, "content")

    return (
      <section className="section">
        <div className="container container--md">
          {title && <h2 className="section__title align-center">{title}</h2>}
          {content && <div className="section__copy">{markdownify(content)}</div>}
        </div>
      </section>
    )
  }
}
