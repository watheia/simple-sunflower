import React, { HTMLAttributes } from "react"
import { get, map, size } from "lodash"

export type BlogPostTagsProps = {
  tags?: any[]
  data?: Record<string, any>
} & HTMLAttributes<HTMLDivElement>

export default class BlogPostTags extends React.Component<BlogPostTagsProps> {
  render() {
    const tags = get(this.props, "tags")
    const tagsLength = size(tags)
    if (!tagsLength) {
      return null
    }

    return (
      <footer className="post__footer">
        {map(tags, (tag, index) => (
          <span key={index}>{tag}</span>
        ))}
      </footer>
    )
  }
}
