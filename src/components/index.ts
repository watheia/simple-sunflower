import Action from "./Action"
import BlogFeedSection from "./BlogFeedSection"
import BlogPostCategories from "./BlogPostCategories"
import BlogPostMeta from "./BlogPostMeta"
import BlogPostTags from "./BlogPostTags"
import ContactSection from "./ContactSection"
import ContentSection from "./ContentSection"
import CtaSection from "./CtaSection"
import FeaturesSection from "./FeaturesSection"
import Footer from "./Footer"
import Header from "./Header"
import HeroSection from "./HeroSection"
import Icon from "./Icon"
import Layout from "./Layout"
import SectionActions from "./SectionActions"
import TeamSection from "./TeamSection"

export const sectionsByTypeName = {
  blog_feed_section: BlogFeedSection,
  contact_section: ContactSection,
  content_section: ContentSection,
  cta_section: CtaSection,
  features_section: FeaturesSection,
  hero_section: HeroSection,
  team_section: TeamSection
}

export {
  Action,
  BlogFeedSection,
  BlogPostCategories,
  BlogPostMeta,
  BlogPostTags,
  ContactSection,
  ContentSection,
  CtaSection,
  FeaturesSection,
  Footer,
  Header,
  HeroSection,
  Icon,
  Layout,
  SectionActions,
  TeamSection
}

export default {
  Action,
  BlogFeedSection,
  BlogPostCategories,
  BlogPostMeta,
  BlogPostTags,
  ContactSection,
  ContentSection,
  CtaSection,
  FeaturesSection,
  Footer,
  Header,
  HeroSection,
  Icon,
  Layout,
  SectionActions,
  TeamSection
}
