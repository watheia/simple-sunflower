import React, { HTMLAttributes } from "react"
import { get, trim } from "lodash"
import moment from "moment-strftime"

import { getData, Link, withPrefix } from "@watheia/utils.stackbit"

export type BlogPostMetaProps = {
  data?: any
  post?: any
  containerClass?: any
} & HTMLAttributes<HTMLDivElement>

export default class BlogPostMeta extends React.Component<BlogPostMetaProps> {
  renderAuthor(
    authorRef: string | undefined,
    data: Record<string, any>
  ): JSX.Element | null {
    const author = getData(data, authorRef)
    if (!author) {
      return null
    }
    const authorName = trim(`${author.first_name} ${author.last_name}`)
    if (author.link) {
      return (
        <span>
          {" "}
          by <Link href={withPrefix(author.link)}>{authorName}</Link>
        </span>
      )
    } else {
      return <span> by {authorName}</span>
    }
  }

  render(): JSX.Element {
    const data = get(this.props, "data")
    const post = get(this.props, "post")
    const date = get(post, "date")
    const dateTimeAttr = moment(date).strftime("%Y-%m-%d %H:%M")
    const formattedDate = moment(date).strftime("%B %d, %Y")
    const authorRef = get(post, "author")
    const containerClass = get(this.props, "containerClass", "")

    return (
      <div className={containerClass}>
        <span>
          On <time dateTime={dateTimeAttr}>{formattedDate}</time>
        </span>
        {authorRef && this.renderAuthor(authorRef, data)}
      </div>
    )
  }
}
