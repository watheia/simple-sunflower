import React, { HTMLAttributes } from "react"
import { map, get, find, isEmpty, orderBy } from "lodash"

import { Link, withPrefix, getData, getPageUrl } from "@watheia/utils.stackbit"
import BlogPostCategories from "@components/BlogPostCategories"
import BlogPostMeta from "@components/BlogPostMeta"

export type BlogFeedSectionProps = {
  data?: any
  section?: any
  post?: any
} & HTMLAttributes<HTMLDivElement>

export default class BlogFeedSection extends React.Component {
  renderBlogFeedItemFilter(
    post: any,
    data: Record<string, any>,
    section: any
  ): JSX.Element | null {
    const sectionTitle = get(section, "title")
    const sectionAuthorRef = get(section, "author")
    const sectionCategoryRef = get(section, "category")
    if (sectionAuthorRef) {
      const sectionAuthor = getData(data, sectionAuthorRef)
      if (isEmpty(sectionAuthor)) {
        return null
      }
      const postAuthorRef = get(post, "author")
      if (!postAuthorRef) {
        return null
      }
      const postAuthor = getData(data, postAuthorRef)
      if (isEmpty(postAuthor)) {
        return null
      }
      if (postAuthor.id === sectionAuthor.id) {
        return this.renderBlogFeedItem(post, data, sectionTitle)
      }
    } else if (sectionCategoryRef) {
      const sectionCategory = getData(data, sectionCategoryRef)
      if (isEmpty(sectionCategory)) {
        return null
      }
      const postCategoryRefs = get(post, "categories")
      const postCategories = map(postCategoryRefs, (postCategoryRef) => {
        return getData(data, postCategoryRef)
      })
      const category = find(postCategories, { id: sectionCategory.id })
      if (category) {
        return this.renderBlogFeedItem(post, data, sectionTitle)
      }
    } else {
      return this.renderBlogFeedItem(post, data, sectionTitle)
    }
    return null
  }

  renderBlogFeedItem(post: any, data: Record<string, any>, sectionTitle: any): JSX.Element {
    const postUrl = getPageUrl(post, { withPrefix: true })
    const title = get(post, "title")
    const image = get(post, "image")
    const imageAlt = get(post, "image_alt", "")
    const categories = get(post, "categories", [])
    const excerpt = get(post, "excerpt")

    return (
      <article className="cell">
        <div className="card">
          {image && (
            <Link className="card__media card__media--top" href={postUrl}>
              <img src={withPrefix(image)} alt={imageAlt} />
            </Link>
          )}
          <div className="card__body">
            <header className="card__header">
              <BlogPostCategories
                categories={categories}
                data={data}
                containerClass={"card__meta"}
              />
              {sectionTitle ? (
                <h3 className="h4 card__title">
                  <Link href={postUrl}>{title}</Link>
                </h3>
              ) : (
                <h2 className="h4 card__title">
                  <Link href={postUrl}>{title}</Link>
                </h2>
              )}
            </header>
            {excerpt && (
              <div className="card__copy">
                <p>{excerpt}</p>
              </div>
            )}
            <BlogPostMeta post={post} data={data} containerClass={"card__footer"} />
          </div>
        </div>
      </article>
    )
  }

  render() {
    const data = get(this.props, "data")
    const section = get(this.props, "section")
    const title = get(section, "title")
    const showRecent = get(section, "show_recent")
    const recentCount = get(section, "recent_count", 3)
    let posts = orderBy(get(this.props, "posts", []), "date", "desc")
    if (showRecent) {
      posts = posts.slice(0, recentCount)
    }

    return (
      <section className="section section--posts">
        {title && (
          <div className="container container--md align-center">
            <h2 className="section__title">{title}</h2>
          </div>
        )}
        <div className="container container--lg">
          <div className="flex flex--col-3">
            {map(posts, (post, index) => {
              return (
                <React.Fragment key={index}>
                  {this.renderBlogFeedItemFilter(post, data, section)}
                </React.Fragment>
              )
            })}
          </div>
        </div>
      </section>
    )
  }
}
