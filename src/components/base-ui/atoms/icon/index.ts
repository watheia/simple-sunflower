import { Icon, IconProps } from "./icon"

export { Icon }
export type { IconProps }
export default Icon
