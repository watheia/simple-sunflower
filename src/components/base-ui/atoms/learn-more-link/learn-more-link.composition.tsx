import React from "react"
import { Box } from "@material-ui/core"
import LearnMoreLink from "."

export const LearnMoreLinkExample = (): JSX.Element => (
  <Box
    display="flex"
    justifyContent="space-between"
    alignItems="center"
    padding={2}
    border="1px solid #ccc"
    borderRadius="4px"
  >
    <LearnMoreLink title="learn more" />
  </Box>
)
