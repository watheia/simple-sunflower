import { LearnMoreLink, LearnMoreLinkProps } from "./learn-more-link"

export { LearnMoreLink }
export type { LearnMoreLinkProps }
export default LearnMoreLink
