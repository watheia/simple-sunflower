import React from "react"
import { sansFont, serifFont } from "./index"

export const SansFont = () => (
  <div className={sansFont}>
    <span>This is the sansFont.</span>
  </div>
)

export const SerifFont = () => (
  <div className={serifFont}>
    <span>This is the sansFont.</span>
  </div>
)

export const WithoutFont = () => (
  <div>
    <span>This is the default font.</span>
  </div>
)
