import styles from "./adobe-clean.module.scss"
const { sansFont, serifFont } = styles
export { sansFont, serifFont }
