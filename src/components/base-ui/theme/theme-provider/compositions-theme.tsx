import React from "react"
import { ThemeProvider } from "./theme-provider"

export function ThemeCompositions(props: any) {
  return <ThemeProvider {...props}>{props.children}</ThemeProvider>
}
