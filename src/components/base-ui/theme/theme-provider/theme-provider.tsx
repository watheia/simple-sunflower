/** @format */

import React, { HTMLAttributes } from "react"
import cn from "classnames"
import { MuiThemeProvider, CssBaseline } from "@material-ui/core"
import { createMuiTheme, ColorScheme, Theme } from "./theme"
import { headingFontSize, textFontSize } from "@waweb/base-ui.theme.size-definition"
import { shadowTheme } from "@waweb/base-ui.theme.shadow-definition"
import { primaryPalette } from "@waweb/base-ui.theme.color-definition"
import { brands } from "@waweb/base-ui.theme.brand-definition"
import { headingMargins } from "@waweb/base-ui.theme.heading-margin-definition"
import texts from "./texts.module.scss"
import { IconFont } from "@waweb/base-ui.theme.icons-font"
import { OverlayProvider, SSRProvider } from "react-aria"
import { ResizeHandler } from "@waweb/base-ui.utils.hooks.resize-handler"
import { NProgress } from "@waweb/base-ui.utils.hooks.nprogress"

export type ThemeProviderProps = {
  /**
   * primary color of theme.
   */
  colorScheme?: ColorScheme
} & HTMLAttributes<HTMLDivElement>

export function ThemeProvider({ colorScheme, children, ...props }: ThemeProviderProps) {
  return (
    <SSRProvider>
      <OverlayProvider>
        <div
          {...props}
          className={cn(
            headingFontSize,
            textFontSize,
            shadowTheme,
            primaryPalette,
            brands,
            headingMargins,
            texts.defaults,
            props.className
          )}
        >
          <IconFont query="eo46cx" />
          <Theme.Provider value={{ colorScheme }}>
            <CssBaseline />
            <MuiThemeProvider theme={createMuiTheme(colorScheme)}>
              {children}
            </MuiThemeProvider>
          </Theme.Provider>
        </div>
        <ResizeHandler />
        <NProgress />
      </OverlayProvider>
    </SSRProvider>
  )
}

//export alias for background compact
export default ThemeProvider
