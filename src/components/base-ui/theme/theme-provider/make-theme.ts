import * as createPalette from "@material-ui/core/styles/createPalette"
import { createTheme, responsiveFontSizes, Theme } from "@material-ui/core"
import { light, dark } from "./palette"
import { ThemeMode } from "./theme-mode"

declare module "@material-ui/core/styles/createPalette" {
  interface TypeBackground {
    paper: string
    default: string
    level2: string
    level1: string
    footer: string
  }

  interface PaletteOptions {
    cardShadow?: string
    alternate: {
      main: string
      dark: string
    }
  }

  interface Palette {
    cardShadow?: string
    alternate: {
      main: string
      dark: string
    }
  }
}

declare module "@material-ui/core/styles/createTheme" {
  interface Theme {
    layout: {
      contentWidth: number | string
    }
  }
  // allow configuration using `createTheme`
  interface ThemeOptions {
    layout?: {
      contentWidth: number | string
    }
  }
}

export const makeTheme = (mode: ThemeMode): Theme =>
  responsiveFontSizes(
    createTheme({
      palette: mode === "light" ? light : dark,
      layout: {
        contentWidth: 1236
      },
      typography: {
        fontFamily: "Lato"
      },
      zIndex: {
        appBar: 1200,
        drawer: 1100
      }
    })
  )
