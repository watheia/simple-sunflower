// eslint-disable-next-line
import { PaletteType } from "@material-ui/core"

export const light = {
  alternate: {
    main: "rgb(247, 249, 250)",
    dark: "#e8eaf6"
  },
  cardShadow: "rgba(23, 70, 161, .11)",
  type: "light" as PaletteType,
  primary: {
    main: "#215C94",
    contrastText: "#fff"
  },
  secondary: {
    main: "#F2B705",
    contrastText: "rgba(0, 0, 0, 0.87)"
  },
  text: {
    primary: "#231F20",
    secondary: "#092947"
  },
  divider: "rgba(0, 0, 0, 0.12)",
  background: {
    paper: "#F3F4F6",
    default: "#fff",
    level2: "#f5f5f5",
    level1: "#ededed",
    footer: "#231F20"
  }
}

export const dark = {
  type: "dark" as PaletteType,
  alternate: {
    main: "#2D3748",
    dark: "#24242b"
  },
  cardShadow: "rgba(0, 0, 0, .11)",
  common: {
    black: "#000",
    white: "#fff"
  },
  secondary: {
    main: "#215C94",
    contrastText: "#fff"
  },
  primary: {
    main: "#F2B705",
    contrastText: "rgba(0, 0, 0, 0.87)"
  },
  text: {
    primary: "#F3F4F6",
    secondary: "#cccfd4"
  },
  divider: "rgba(255, 255, 255, 0.12)",
  background: {
    paper: "#231F20",
    default: "#222222",
    level2: "#333",
    level1: "#2D3748",
    footer: "#231F20"
  }
}
