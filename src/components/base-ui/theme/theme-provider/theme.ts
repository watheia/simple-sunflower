import { createContext } from "react"
import { createTheme, responsiveFontSizes } from "@material-ui/core"
import { light, dark } from "./palette"

export const contentWidth = 1236

export type ColorScheme = "light" | "dark"

export type ThemeType = {
  /**
   * primary color of theme.
   */
  colorScheme?: ColorScheme
}

export const Theme = createContext<ThemeType>({
  colorScheme: "light"
})

/** @format */

export const createMuiTheme = (mode: ColorScheme = "dark") =>
  responsiveFontSizes(
    createTheme({
      palette: mode === "light" ? light : dark,
      layout: {
        contentWidth
      },
      typography: {
        fontFamily: "Adobe Clean"
      },
      zIndex: {
        appBar: 1200,
        drawer: 1100
      }
    })
  )
