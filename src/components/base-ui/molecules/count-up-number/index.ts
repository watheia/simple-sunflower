import { CountUpNumber, CountUpNumberProps } from "./count-up-number"

export { CountUpNumber }
export type { CountUpNumberProps }
export default CountUpNumber
