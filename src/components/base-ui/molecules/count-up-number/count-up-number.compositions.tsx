import React from "react"
import { Box } from "@material-ui/core"
import CountUpNumber from "."

export function Example() {
  return (
    <Box
      marginBottom={2}
      padding={2}
      border="1px solid #ccc"
      borderRadius="4px"
      overflow="auto"
    >
      <Box
        display="flex"
        justifyContent="space-around"
        alignItems="center"
        minWidth="600px"
      >
        <CountUpNumber end={458} suffix="K" label="Placement" />
        <CountUpNumber end={360} prefix="$" suffix="K" label="Money Invested" />
        <CountUpNumber end={80} suffix="+" label="Locations" />
      </Box>
    </Box>
  )
}
