import MenuItemProps from "./MenuItemProps"

export default interface MenuGroupProps {
  item: MenuItemProps
}
