export default interface MenuItemProps {
  groupTitle: string
  pages: Array<{ href: string; title: string }>
}
