import MenuItemProps from "./MenuItemProps"

// TODO remove this temporary placeholder setup during migration
export default interface NavigationProps {
  landings: {
    title: string
    id: string
    children: {
      services: MenuItemProps
      apps: MenuItemProps
      web: MenuItemProps
    }
  }
  pages: {
    title: string
    id: string
    children: {
      career: MenuItemProps
      helpCenter: MenuItemProps
      company: MenuItemProps
      contact: MenuItemProps
      blog: MenuItemProps
      portfolio: MenuItemProps
    }
  }
  account: {
    title: string
    id: string
    children: {
      settings: MenuItemProps
      signup: MenuItemProps
      signin: MenuItemProps
      password: MenuItemProps
      error: MenuItemProps
    }
  }
}
