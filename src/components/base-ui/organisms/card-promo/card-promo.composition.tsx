import React from "react"
import { Box, Grid, colors } from "@material-ui/core"
import CardPromo from "."

export function Example1() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <CardPromo
            title="1000+"
            subtitle="Online Courses"
            description="Choose from over 1000+ online video courses."
            fontIconClass="fas fa-pen-nib"
            color={colors.pink}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={4}>
          <CardPromo
            title="800+"
            subtitle="Online Courses"
            description="Choose from over 1000+ online video courses."
            fontIconClass="fas fa-book-open"
            color={colors.purple}
            variant="outlined"
          />
        </Grid>
        <Grid item xs={4}>
          <CardPromo
            title="100K+"
            subtitle="Online Courses"
            description="Choose from over 1000+ online video courses."
            fontIconClass="fas fa-camera-retro"
            color={colors.blue}
            variant="outlined"
          />
        </Grid>
      </Grid>
    </Box>
  )
}

export function Example2() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <CardPromo
            title="1000+"
            subtitle="Online Courses"
            description="Choose from over 1000+ online video courses."
            fontIconClass="fas fa-pen-nib"
            color={colors.pink}
            withShadow
            liftUp
          />
        </Grid>
        <Grid item xs={4}>
          <CardPromo
            title="800+"
            subtitle="Online Courses"
            description="Choose from over 1000+ online video courses."
            fontIconClass="fas fa-book-open"
            color={colors.purple}
            withShadow
            liftUp
          />
        </Grid>
        <Grid item xs={4}>
          <CardPromo
            title="100K+"
            subtitle="Online Courses"
            description="Choose from over 1000+ online video courses."
            fontIconClass="fas fa-camera-retro"
            color={colors.blue}
            withShadow
            liftUp
          />
        </Grid>
      </Grid>
    </Box>
  )
}

export function Example3() {
  return (
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <CardPromo
            title="1000+"
            subtitle="Online Courses"
            description="Choose from over 1000+ online video courses."
            fontIconClass="fas fa-pen-nib"
            color={colors.pink}
            withShadow
            liftUp
            align="center"
          />
        </Grid>
        <Grid item xs={4}>
          <CardPromo
            title="800+"
            subtitle="Online Courses"
            description="Choose from over 1000+ online video courses."
            fontIconClass="fas fa-book-open"
            color={colors.purple}
            withShadow
            liftUp
            align="center"
          />
        </Grid>
        <Grid item xs={4}>
          <CardPromo
            title="100K+"
            subtitle="Online Courses"
            description="Choose from over 1000+ online video courses."
            fontIconClass="fas fa-camera-retro"
            color={colors.blue}
            withShadow
            liftUp
            align="center"
          />
        </Grid>
      </Grid>
    </Box>
  )
}
