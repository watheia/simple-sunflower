import React from "react"
import { Box, Button } from "@material-ui/core"
import Section from "@waweb/base-ui.organisms.section"
import SectionHeader from "@waweb/base-ui.molecules.section-header"
import HeroSimpleBackground from "."

export const HeroSimpleBackgroundComposition = ({ ...rest }: any): JSX.Element => (
  <div {...rest}>
    <Box marginBottom={2} padding={2} border="1px solid #ccc" borderRadius="4px">
      <HeroSimpleBackground backgroundImage="https://cdn.watheia.org/assets/shapes/banner-bg.svg">
        <Section narrow>
          <SectionHeader
            title="Supercharge Your Web Product's UI/UX Design"
            titleVariant="h2"
            subtitle="Our mission is to help you to grow your design skills, meet and connect with professional dsigners who share your passions. We help you fulfill your best potential through an engaging lifestyle experience."
            ctaGroup={[
              <Button key={0} color="primary" variant="contained" size="large">
                Try for free
              </Button>,
              <Button key={1} color="primary" variant="outlined" size="large">
                See pricing
              </Button>
            ]}
          />
        </Section>
      </HeroSimpleBackground>
    </Box>
  </div>
)

export default HeroSimpleBackgroundComposition
