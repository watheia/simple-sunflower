import React from "react"
import { render } from "@testing-library/react"
import { BasicMinimal } from "./minimal.composition"

it("should render with the correct text", () => {
  const { getByText } = render(<BasicMinimal />)
  const rendered = getByText("hello from Minimal")
  expect(rendered).toBeTruthy()
})
