import React from "react"
import clsx from "clsx"
import { makeStyles } from "@material-ui/core/styles"
import { Divider } from "@material-ui/core"
import { Topbar } from "./components"
import { HTMLAttributes } from "react"

const useStyles = makeStyles(() => ({
  root: {},
  content: {
    height: "100%"
  }
}))

type Props = {
  children: React.ReactNode
  themeMode: string
  className?: string
} & HTMLAttributes<HTMLDivElement>

const Minimal = ({ themeMode, children, className, ...rest }: Props): JSX.Element => {
  const classes = useStyles()

  return (
    <div className={clsx(classes.root, className)} {...rest}>
      <Topbar themeMode={themeMode} />
      <Divider />
      <main className={classes.content}>{children}</main>
    </div>
  )
}

export default Minimal
