import React from "react"
import Minimal from "./minimal"

export const BasicMinimal = () => (
  <Minimal themeMode="light">
    <p>hello from Minimal</p>
  </Minimal>
)
