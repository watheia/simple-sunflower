import React from "react"
import { render } from "@testing-library/react"
import NProgress from "./nprogress"

it("should render without crashing", () => {
  const { getByTestId } = render(
    <div data-testid="nprogress">
      <NProgress />
    </div>
  )
  const rendered = getByTestId("nprogress")
  expect(rendered).toBeTruthy()
})
