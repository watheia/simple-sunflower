import React from "react"
import { render } from "@testing-library/react"
import ResizeHandler from "./resize-handler"

it("should render without crashing", () => {
  const { getByTestId } = render(
    <div data-testid="resize-handler">
      <ResizeHandler />
    </div>
  )
  const rendered = getByTestId("resize-handler")
  expect(rendered).toBeTruthy()
})
