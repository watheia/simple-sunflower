---
title: Home
sections:
  - type: hero_section
    title: We build products your customers will love
    subtitle: >-
      Watheia Labs, LLC is a modern engineering and design agency providing technical support services from Silicon Valley to Seattle. We offer businesses both strategic and tactical approaches to building digital products the right way.
    actions:
      - label: Get Support
        url: /contact?support
        style: primary
    align: left
    image_alt: Hero placeholder image
    image_position: right
    has_background: true
    background:
      background_color: blue
      background_image: /images/background-80.jpg
      background_image_opacity: 100
      background_image_size: auto
      background_image_repeat: repeat
    # image: /images/iconxhdpi.png
  - type: features_section
    title: Case Studies
    features:
      - title: Do more with less
        content: >-
          Connect all teams and projects with your component system to make sure your entire organization builds and ships together the same way.
        align: left
        image: /images/graphs-enterprise.svg
        image_alt: Feature 1 placeholder image
        image_position: right
        actions:
          - label: Learn More
            url: /features
            style: secondary
      - title: Deliver Features Faster
        content: >-
          Watheia Labs empowers teams to deliver more often and faster to production while ensuring all applications and teams deploy in perfect sync and harmony.
        align: left
        image: /images/automated-github.png
        image_alt: Feature 2 placeholder image
        image_position: left
        actions:
          - label: Learn More
            url: /features
            style: secondary
      - title: Increase Customer Focus
        content: >-
          Let your ideal customer drive web application behavior and delivery at global scale. Enjoy world-class performance, security and support every step of the way.
        align: left
        image: /images/component-community.png
        image_alt: Feature 3 placeholder image
        image_position: right
        actions:
          - label: Learn More
            url: /features
            style: secondary
  - type: blog_feed_section
    title: Featured Posts
    show_recent: true
    recent_count: 3
  - type: cta_section
    title: Let's work together
    subtitle: >-
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam a metus
      quis lorem malesuada luctus.
    actions:
      - label: Get Support
        url: /contact
        style: primary
    has_background: true
    background_color: gray
seo:
  title: Micro frontends | by Watheia Labs
  description: We build micro frontends!
  extra:
    - name: "og:type"
      value: website
      keyName: property
    - name: "og:title"
      value: Stackbit Starter Theme
      keyName: property
    - name: "og:description"
      value: We build micro frontends!
      keyName: property
    - name: "og:image"
      value: images/starter-preview.png
      keyName: property
      relativeUrl: true
    - name: "twitter:card"
      value: summary_large_image
    - name: "twitter:title"
      value: Stackbit Starter Theme
    - name: "twitter:description"
      value: We build micro frontends!
    - name: "twitter:image"
      value: images/starter-preview.png
      relativeUrl: true
layout: advanced
---
