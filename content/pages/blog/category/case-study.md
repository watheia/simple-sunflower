---
title: Tutorials
sections:
  - type: hero_section
    title: All Case Studies
    align: center
  - type: blog_feed_section
    show_recent: false
    category: content/data/categories/case-study.yaml
seo:
  title: Case Studies
  description: This is the category archive page
  extra:
    - name: "og:type"
      value: website
      keyName: property
    - name: "og:title"
      value: Case Studies
      keyName: property
    - name: "og:description"
      value: This is the category archive page
      keyName: property
    - name: "og:image"
      value: images/1.png
      keyName: property
      relativeUrl: true
    - name: "twitter:card"
      value: summary_large_image
    - name: "twitter:title"
      value: Case Studies
    - name: "twitter:description"
      value: This is the category archive page
    - name: "twitter:image"
      value: images/1.png
      relativeUrl: true
layout: advanced
---
